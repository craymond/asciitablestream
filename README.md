# C++ classes to manage table with streams

version 0.5.0

- manage console ascii tables
- use `operator<<` for each column
- and `asciitablestream::endl` to mark the end of tabline
- use `asciitablestream::Alignment::RIGHT`, `asciitablestream::Alignment::LEFT` or `asciitablestream::Alignment::CENTER`  to align a specific line
- provide latex code
- export to CSV
- can print 2 ascii tables side to side

C++ example:

```cpp
#include "asciitablestream.hpp"
#include <iostream>
#include <string>
#include <cmath>

int main()
{
    std::cout << "Right Aligned Table"<<'\n';
    
    asciitablestream rat;
    rat << "name"      << "size(m)" << "weigth(kg)" << asciitablestream::endl;
    rat << "Me"        << 1.790     <<          97  << asciitablestream::endl;
    rat << "Le Banner" << 1.9       <<         120  << asciitablestream::endl;
    std::cout << rat<<'\n';

    std::cout << "Table with manual config: first column left aligned and other at rigth"<<'\n';
    
    asciitablestream latm(asciitablestream::Alignment::RIGHT,asciitablestream::Style::DOUBLE,3,true);
    latm<< asciitablestream::Alignment::LEFT<< "name"<<"size(m)"<<"weigth(kg)"<<"healthy"<<asciitablestream::endl;
    latm<< "Me"<<1.790<<97<<false<<asciitablestream::endl;
    latm<< "Jerome Le Banner"<<1.9<<120<<true<<asciitablestream::endl<< "Atchoum"<<1.12<<35<<false<<asciitablestream::endl;
    std::cout << latm<<'\n';

    std::cout << "Table printed step by step"<<'\n';

    asciitablestream tpsbs(asciitablestream::Alignment::LEFT,asciitablestream::Style::LIGHT,3,false);
    tpsbs << "number" << asciitablestream::Alignment::RIGHT << "square root" << asciitablestream::endl;
    std::cout << tpsbs.head();
    for(int i=0;i<3;++i)
    {
        tpsbs << i << sqrt(i) << asciitablestream::endl;
        std::cout << tpsbs.current(i==2);//true for the last iter: print the last line of the table
    }

    std::cout << "\nLatex Table"<<'\n';
    asciitablestream latex(asciitablestream::Alignment::RIGHT);
    latex<< "name"<<"size(m)"<<"weigth(kg)"<<"healthy"<<asciitablestream::endl;
    latex<< "Me"<<1.790<<97<<false<<asciitablestream::endl;
    latex<< "Jerome Le Banner"<<1.9<<120<<true<<asciitablestream::endl<< "Atchoum"<<1.12<<35<<false<<asciitablestream::endl;
    std::cout << latex.toLatex()<<'\n';

    std::cout << "\nPrint 2 tables side to side\n";
    std::cout << rat.toStringBeside(rat,"\t=>\t");

    return EXIT_SUCCESS;
}
```

```markdown
Right Aligned Table
┌───────────┬─────────┬────────────┐
│      name │ size(m) │ weigth(kg) │
├───────────┼─────────┼────────────┤
│        Me │    1.79 │         97 │
├───────────┼─────────┼────────────┤
│ Le Banner │    1.90 │        120 │
└───────────┴─────────┴────────────┘

Table with manual config: first column left aligned and other at right
╔══════════════════╦═════════╦════════════╦═════════╗
║ name             ║ size(m) ║ weigth(kg) ║ healthy ║
╠══════════════════╬═════════╬════════════╬═════════╣
║ Me               ║   1.790 ║         97 ║   false ║
╠══════════════════╬═════════╬════════════╬═════════╣
║ Jerome Le Banner ║   1.900 ║        120 ║    true ║
╠══════════════════╬═════════╬════════════╬═════════╣
║ Atchoum          ║   1.120 ║         35 ║   false ║
╚══════════════════╩═════════╩════════════╩═════════╝

Table printed step by step
+--------+-------------+
| number | square root |
+--------+-------------+
| 0      |       0.000 |
| 1      |       1.000 |
| 2      |       1.414 |
+--------+-------------+

Latex Table
\begin{table}{|r|r|r|r|}
\\hline
name & size(m) & weigth(kg) & healthy & healthy\\\hline
Me & 1.79 & 97 & false & false\\\hline
Jerome Le Banner & 1.90 & 120 & true & true\\\hline
Atchoum & 1.12 & 35 & false & false\hline
\end{table}

Print 2 tables side to side
┌──────────────────┬─────────┬────────────┐     =>      ┌──────────────────┬─────────┬────────────┐
│             name │ size(m) │ weigth(kg) │     =>      │             name │ size(m) │ weigth(kg) │
├──────────────────┼─────────┼────────────┤     =>      ├──────────────────┼─────────┼────────────┤
│               Me │    1.79 │         97 │     =>      │               Me │    1.79 │         97 │
├──────────────────┼─────────┼────────────┤     =>      ├──────────────────┼─────────┼────────────┤
│ Jerome Le Banner │    1.90 │        120 │     =>      │ Jerome Le Banner │    1.90 │        120 │
└──────────────────┴─────────┴────────────┘     =>      └──────────────────┴─────────┴────────────┘
```
