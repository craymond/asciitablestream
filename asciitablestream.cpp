/**
 * \file asciitablestream.cpp
 * \brief class definition
 * \author Christian Raymond
 * \version 0.2
 * \date 25 octobre 2020
 *
 * class to manage ascii table using stream operators
 *
 */
#include "asciitablestream.hpp"

size_t asciitablestream::utf8Len(const std::string& s)
{
  return std::count_if(s.begin(), s.end(),[](const unsigned char c) { return (c & 0xC0) != 0x80; } );
}

asciitablestream::car asciitablestream::Theme::get(const Table pos,const Side side) const
{
	return _structure[pos][side];
}

const std::unordered_map<asciitablestream::Style,asciitablestream::Theme> asciitablestream::LIST_OF_STYLES =
{
	{Style::EMPTY,
		{
			{
				{" "," "," "},
				{" "," "," "},
				{" "," "," "}
			},
			" "," "
		}
	},
	{Style::LIGHT,
		{
			{
				{"+","+","+"},
				{"+","+","+"},
				{"+","+","+"}
			},
			"-","|"
		}
	},
	{Style::SIMPLE,
		{
			{
				{"┌","┬","┐"},
				{"├","┼","┤"},
				{"└","┴","┘"}
			},
			"─","│"
		}
	},
	{Style::ONLYLINES,
		{
			{
				{"_","_","_"},
				{"_","_","_"},
				{"_","_","_"}
			},
			"_"," "
		}
	},
	{Style::DOUBLE,
		{
			{
				{"╔","╦","╗"},
				{"╠","╬","╣"},
				{"╚","╩","╝"}
			},
			"═","║"
		}
	}
};

asciitablestream::Style asciitablestream::_backoff_style=asciitablestream::Style::UNSETTED; //style to use if the style is not provided by the constructor

void asciitablestream::set_style(const Style s)
{	
	_backoff_style=s;
}

asciitablestream::asciitablestream(const Alignment da, const Style st,cuint rp,const bool sepline) :
	_defaultAlignment(da),
	_reelprecision(rp),
	_separatorLine(sepline),
	_style(st)
{
	if(_style==Style::UNSETTED)
	{		
		if(_backoff_style==Style::UNSETTED)
			_style=Style::SIMPLE; //if no global nor local style provided: default SIMPLE
		else
			_style=_backoff_style;
	}

}

asciitablestream::uint asciitablestream::nbColumns() const
{
	return std::max_element(_rows.begin(),_rows.end(),[](const auto& l1,const auto& l2){return l1.size()<l2.size();})->size();	
}

asciitablestream::uint asciitablestream::determineWidths() const
{
	_width.assign(this->nbColumns(), 0);
	for (const auto& row: _rows) 
	{		
		for (uint i = 0; i < row.size(); ++i) 
		{
			const uint len=utf8Len(row[i]);
			//const uint len=row[i].size();
			_width[i] = std::max(_width[i],len);	
		}
	}
	return std::accumulate(_width.cbegin(),_width.cend(),this->nbColumns()*3+1);
}

std::string asciitablestream::repeat(cuint times, car c)
{
	std::ostringstream out;
	for(uint i=0;i<times;++i) out<<c; 
	return out.str();
	//return std::string(times, c);
}

std::string asciitablestream::traceLine(const Table pos) const
{
	std::string result;
	result +=  LIST_OF_STYLES.at(_style).get(pos,LEFT);
	for (uint i=0;i<_width.size()-1;++i) 
	{
		result += asciitablestream::repeat(_width[i]+2, LIST_OF_STYLES.at(_style)._horizontal);//+2 car je met un espace avant et après le contenu des cases
		result += LIST_OF_STYLES.at(_style).get(pos,MIDDLE);
	}
	result += asciitablestream::repeat(_width.back()+2, LIST_OF_STYLES.at(_style)._horizontal);//+2 car je met un espace avant et après le contenu des cases
	result += LIST_OF_STYLES.at(_style).get(pos,RIGHT);

	return result;
}



asciitablestream& asciitablestream::operator<<(const Alignment a)
{
	_alignment[_current.size()]=a;
	return *this;
}

asciitablestream::Alignment asciitablestream::alignment(cuint i) const
{
	auto res = this->_alignment.find(i);
	if (res == _alignment.end())
		return _defaultAlignment;
	return res->second;
}

std::string asciitablestream::row(cuint r,const Table pos) const
{
	std::ostringstream stream;
	const char space = ' ';
	stream << LIST_OF_STYLES.at(_style)._vertical;
	for (uint i = 0; i < _rows[r].size()-1; ++i)
	{
		const auto pad=repeat(_width[i]-utf8Len(_rows[r][i])," ");
		switch(this->alignment(i))
		{
			case Alignment::LEFT: stream << space << _rows[r][i] << pad; break;		
			case Alignment::RIGHT: stream << space << pad << _rows[r][i];break;
			case Alignment::CENTER: stream << space << pad.substr(pad.size()/2,pad.size()) << _rows[r][i] << pad.substr(0,pad.size()/2);  break;
									
		}
		stream << space << LIST_OF_STYLES.at(_style)._vertical;
	}
	auto alignment = this->alignment(_rows[r].size()-1) == asciitablestream::Alignment::LEFT ? std::left : std::right;
	stream << space << std::setw(_width.back()) << alignment << _rows[r].back();
	stream << space << LIST_OF_STYLES.at(_style)._vertical;
	return stream.str();
}

std::string asciitablestream::current(const bool end) const
{
	if(end) return this->row(_rows.size()-1) +'\n' + this->traceLine(END) + '\n';
	if(_separatorLine) 	return this->row(_rows.size()-1) + '\n' + this->traceLine() + '\n';
	return this->row(_rows.size()-1) + '\n';
}

std::string asciitablestream::head() const
{
	this->determineWidths();	
	return this->traceLine(HEAD) + "\n" + this->row(0) + "\n" + this->traceLine() + "\n";
}

std::string asciitablestream::toString() const
{
	this->determineWidths();
	if(_rows.empty()) return ""; //empty table
	if(_rows.size()==1)  return this->head(); 
	
	std::ostringstream stream;
	stream << this->head();
	for (uint i=1;i<_rows.size()-1;++i)
	{
		stream << this->row(i) << '\n';
		if(_separatorLine) 
			stream << this->traceLine() << '\n';	
	}
	stream << this->row(_rows.size()-1) << '\n'; 
	stream << this->traceLine(END) << '\n';
	
	return stream.str();
}

std::string asciitablestream::toStringBeside(const asciitablestream& r,const std::string& separator,std::string title1,std::string title2) const
{	
	const std::string gfill(this->determineWidths(),' ');
	r.determineWidths();

	if(_rows.empty()) throw std::runtime_error("empty table");
	//if(_rows.size()==1)  return this->row(0); 
		
	std::ostringstream stream;
	if(!title1.empty() || !title2.empty())
	{
		auto sf=[](std::string& s,cuint size)->std::string 
		{
			
			if(const auto icut=s.find_first_of('\n'); icut!=std::string::npos && icut<s.size())
			{
				const auto rest=s.substr(icut+1); 
				s.erase(icut); 
				return rest;
			}
			if(s.size()>size)
			{ 
				const auto rest=s.substr(size); 
				s.erase(size); 
				return rest;
			} 
			return "";
		};
		std::string restg,restd;
		do
		{
		restg=sf(title1,gfill.size());
		restd=sf(title2,r.determineWidths());
		std::string fillg(std::max<int>(0,(gfill.size()-title1.size())/2),' ');
		std::string filld(std::max<int>(0,(r.determineWidths()-title2.size())/2),' ');
		stream << fillg << title1 << fillg << separator << filld << title2<<"\n";
		title1=restg;
		title2=restd;
		}
		while(!restg.empty() || !restd.empty());
	}

	stream << this->traceLine(HEAD) + separator + r.traceLine(HEAD) + "\n";
	stream << this->row(0) << separator << r.row(0) << '\n';
	stream << this->traceLine() + separator + r.traceLine() + "\n";
	uint ig,id;
	
	for (ig=id=1;ig<_rows.size() && id<r._rows.size();)
	{
		stream << this->row(ig++) << separator << r.row(id++) << '\n';
		
		const Table g=ig==_rows.size()? END: CENTER;
		const Table d=id==r._rows.size()? END: CENTER;
		
		if(_separatorLine && r._separatorLine) 
		{						
			stream << this->traceLine(g) << separator << r.traceLine(d) + '\n';
		}
		
		if(_separatorLine && !r._separatorLine) 
		{
			if(d!=END)
				stream << this->traceLine(g) << separator << r.row(id++) + '\n' ;
			else
				stream << this->traceLine(g) << separator << r.traceLine(END) + '\n' ;
		}

		if(!_separatorLine && r._separatorLine)
		{
			if(g!=END)
				stream << row(ig++) << separator << r.traceLine(d) + '\n';
			else
				stream << this->traceLine(END) << separator << r.traceLine(d) + '\n' ;
		}

		if(!_separatorLine && !r._separatorLine && g==END && d==END)
			stream << traceLine(END) << separator << r.traceLine(END) << '\n';
							
	}	

	const bool gfinish=ig==_rows.size()? true:false;
	const bool dfinish=id==r._rows.size()? true:false;
	if(gfinish && dfinish)
	{
		//stream << traceLine(END) << separator << r.traceLine(END) << '\n';
		return stream.str();
	}

	if(gfinish) //left table < rigth table
	{
		for (;id<r._rows.size()-1;++id)
		{
			stream << gfill << separator << r.row(id) << '\n';
			if(r._separatorLine)	
				stream << gfill << separator << r.traceLine() << '\n';		
		}
		stream << gfill << separator << r.row(r._rows.size()-1) << '\n';
		stream << gfill << separator << r.traceLine(END) << '\n';
		return stream.str();
	}
	//left table > rigth table
	for (;ig<_rows.size()-1;++ig)
	{
		stream << row(ig)  <<separator <<  '\n';
		if(_separatorLine)	
			stream << traceLine() << '\n';		
	}
	stream << row(_rows.size()-1)  <<separator<< '\n';
	stream << traceLine(END) << separator<< '\n';
	return stream.str();
}

std::string asciitablestream::toCSV(const char* sep) const
{
	std::ostringstream stream;

	for (const auto& r : _rows)
	{
		std::copy(r.begin(),r.end(),std::ostream_iterator<std::string>(stream,sep));	
		stream<<'\n'	;
	}
	return stream.str();
}

std::string asciitablestream::toLatex() const
{
	if(_rows.empty()) return ""; //empty table
	std::string sep="|";
	if (!_separatorLine)
		sep="";
	
	
	std::ostringstream stream;
	stream << "\\begin{table}{|";
	static constexpr char map[]={'l','c','r'};
	for (uint i=0;i<_rows.size()-1;++i)
		stream << map[static_cast<uint>(this->alignment(i))] << sep ;
		
	stream << map[static_cast<uint>(this->alignment(_rows.size()-1))] <<"|}\n\\hline\n";
	std::copy(_rows[0].begin(),_rows[0].end()-1,std::ostream_iterator<std::string>(stream," & "));
	stream << _rows[0].back()<<"\\\\\\hline\n";
	for (uint i=1;i<_rows.size()-1;++i)
	{
		std::copy(_rows[i].begin(),_rows[i].end()-1,std::ostream_iterator<std::string>(stream," & "));
		stream << _rows[i].back()<<"\\\\";
		if(_separatorLine) 
			stream << "\\hline";
		stream << "\n";
	}
	std::copy(_rows.back().begin(),_rows.back().end()-1,std::ostream_iterator<std::string>(stream," & "));
	stream << _rows.back().back()<<"\\\\\\hline\n\\end{table}";	
	
	return stream.str();
}

std::ostream & operator<<(std::ostream & stream, const asciitablestream& table)
{
	stream << table.toString();
	return stream;
}


asciitablestream& asciitablestream::endl(asciitablestream& e)
{
	e._rows.push_back(e._current);
	e._current.clear();
	if(e._rows.size()>1 && e._rows.back().size() != e._rows[0].size())
		throw std::length_error("asciitablestream: number of element in line "+std::to_string(e._rows.size())+" is different from header");
	return e;
}


asciitablestream& asciitablestream::operator<<(asciitablestream& (*pf)(asciitablestream&)) {
return pf(*this);
}

