/**
 * \file asciitablestream.hpp
 * \brief class definition
 * \author Christian Raymond
 * \version 0.2
 * \date 25 octobre 2020
 *
 * class to manage ascii table using stream operators
 *
 */
#ifndef asciitablestream_hpp
#define asciitablestream_hpp
#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>
#include <array>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <concepts>

template<class T>
concept Conteneur =  !std::is_same_v<T, std::string> && requires(T a)
{
   a.begin();
   a.end();
};

template< class T >
concept integral_number = !std::is_same_v<T, bool> && std::is_integral_v<T>;

template< class T >
concept real_number = std::is_floating_point_v<T>;

template< class T >
concept boolean = std::is_same_v<T, bool>;

template< class T >
concept Other = !integral_number<T> && !real_number<T> && !boolean<T> && !Conteneur<T>;


class asciitablestream
{	
	using Row= std::vector<std::string>;
	using uint= unsigned int;
	using cuint= const unsigned int;
	using car=const char*;
public:
	/**
	 * \struct Alignment
	 * \brief text cell alignment
	*/
	enum class Alignment 
	{ 
		LEFT, /*!< LEFT alignment */
		CENTER, /*!< CENTER alignment */
		RIGHT /*!< RIGHT aligment. */
	};
	enum class Style{EMPTY,LIGHT,ONLYLINES,SIMPLE,DOUBLE,UNSETTED}; //Table styles
	static size_t utf8Len(const std::string& s);
private:
	enum Table{HEAD,CENTER,END};
	enum Side {LEFT,MIDDLE,RIGHT};
	struct Theme
	{
		//std::array<std::array<car,3>,3> _structure;
		car _structure[3][3];
		car _horizontal;
		car _vertical;
		
		car get(const Table pos=CENTER,const Side side=LEFT) const;
	};	
	static const std::unordered_map<Style,Theme> LIST_OF_STYLES; //implementation of styles
private:
	Row _current;
	std::vector<Row> _rows;
	std::unordered_map<uint, Alignment > _alignment;//possible different alignment for each column
	const Alignment _defaultAlignment;
	cuint _reelprecision;
	const bool _separatorLine;
	Style _style;
	
	static Style _backoff_style; //back off style
	mutable std::vector<uint>  _width;

	uint nbColumns() const;
	uint determineWidths() const;
	std::string traceLine(const Table pos=CENTER) const;//virtual?
	Alignment alignment(cuint i) const;
	std::string row(cuint i,const Table pos=CENTER) const;//virtual?
	template<typename NUMBER>
	void convertNumber(const NUMBER,cuint prec);
	static std::string repeat(cuint times, car c);
public:

	/**
	 * \fn asciitablestream
	 * \brief visual settings of the table
	 *
	 * \param da alignment in cells (default: RIGHT)
	 * \param s style of table (default: Style::SIMPLE)
	 * \param rp number of digits after comma for numbers (default:'2')
	 * \param sepline draw separation line (default: true)
	 */
	asciitablestream(const Alignment da=Alignment::RIGHT,const Style s=Style::UNSETTED,const uint rp=2,const bool sepline=true);
	std::string toLatex() const;
	std::string toCSV(const char* sep=";") const;
	std::string toString() const;
	std::string current(const bool end=false) const;
	std::string head() const;
	asciitablestream& operator<<(const boolean auto e);	
	asciitablestream& operator<<(const Alignment a);
	asciitablestream& operator<<(const Conteneur auto& t);
	asciitablestream& operator<<(const real_number auto content);
	asciitablestream& operator<<(const integral_number auto content);	
	asciitablestream& operator<<(const Other auto& content);
	//end the line of the table
	static asciitablestream& endl(asciitablestream& os);
	asciitablestream& operator<<(asciitablestream& (*pf)(asciitablestream&));
	static void set_style(const Style s);
	// output two tables side by side
	std::string toStringBeside(const asciitablestream&,const std::string& separator="\t",std::string title1="",std::string title2="") const;
};

std::ostream& operator<<(std::ostream & stream, const asciitablestream& table);


template<typename NUMBER>
void asciitablestream::convertNumber(const NUMBER n,cuint prec)
{
	std::ostringstream out;
	out << std::fixed << std::setprecision(prec) << std::dec << n;
	_current.push_back(out.str());
}



asciitablestream& asciitablestream::operator<<(const real_number auto content)
{
	this->convertNumber(content, _reelprecision);
	return *this;
}

asciitablestream& asciitablestream::operator<<(const integral_number auto e)
{
	this->convertNumber(e, 0);
	return *this;
}

asciitablestream& asciitablestream::operator<<(const boolean auto e)
{
	if (e)
		_current.emplace_back("true");
	else
		_current.emplace_back("false");
		
	return *this;
}


asciitablestream& asciitablestream::operator<<(const Conteneur auto& t)
{
	for(const auto& e : t)
		*this << e;
	return *this;
}


asciitablestream& asciitablestream::operator<<(const Other auto& content)
{
	std::ostringstream out;
	out << content;
	_current.push_back(out.str());
	return *this;
}

#endif