/**
 * \file examples.hpp
 * \brief simple program to illustrate uses
 * \author Christian Raymond
 * \version 0.1
 * \date 8 octobre 2020
 *
 * simple program to illustrate uses
 *
 */
#include "asciitablestream.hpp"
#include <iostream>
#include <string>
#include <cmath>

int main()
{

    const std::vector<std::string> one_ligne={"Jérôme Vector","1.9","120"};

    asciitablestream::set_style(asciitablestream::Style::SIMPLE);
    
    std::cout << "Right Aligned Table"<<'\n';
    asciitablestream rat;
    rat<< "name"<<"size(m)"<<"weigth(kg)"<<asciitablestream::endl;
    rat<< "Me"<<1.790<<97<<asciitablestream::endl;
    rat<< "Jérôme Le Banner"<<1.9<<120<<asciitablestream::endl;
    rat<< one_ligne << asciitablestream::endl;
    std::cout << rat<<'\n';
 
    std::cout << "Table with manual config: first column left aligned and other at right"<<'\n';
    asciitablestream latm(asciitablestream::Alignment::RIGHT,asciitablestream::Style::DOUBLE,3,true);
    latm<< asciitablestream::Alignment::CENTER<< "name"<<"size(m)"<<"weigth(kg)"<<"healthy"<<asciitablestream::endl;
    latm<< "Me"<<1.790<<97<<false<<asciitablestream::endl;
    latm<< "été"<<1.9<<120<<true<<asciitablestream::endl<< "Atchoum"<<1.12<<35<<false<<asciitablestream::endl;
    latm<< "Jérôme Le Béné"<<1.9<<120<<true<<asciitablestream::endl<< "Atchoum"<<1.12<<35<<false<<asciitablestream::endl;
    std::cout << latm<<'\n';

    std::cout << "Table printed step by step"<<'\n';
    asciitablestream tpsbs(asciitablestream::Alignment::LEFT,asciitablestream::Style::LIGHT,3,false);
    tpsbs << "number"<<asciitablestream::Alignment::RIGHT<< "square root" << asciitablestream::endl;
    std::cout<<tpsbs.head();
    for(int i=0;i<3;++i)
    {
        tpsbs<< i<<sqrt(i)<<asciitablestream::endl;
        std::cout<<tpsbs.current(i==2);//true for the last iter: print the last line of the table
    }

    std::cout << "\nLatex or CSV Table\n";
    asciitablestream latex(asciitablestream::Alignment::CENTER,asciitablestream::Style::SIMPLE,2,true);
    latex<< "name"<<"size(m)"<<"weigth(kg)"<<"healthy"<<asciitablestream::endl;
    latex<< "Me"<<1.790<<97<<false<<asciitablestream::endl;
    latex<< "Jerome Le Banner"<<1.95<<120<<true<<asciitablestream::endl<< "Atchoum"<<1.12<<35<<false<<asciitablestream::endl;
    std::cout << latex.toLatex()<<'\n';
    std::cout << latex.toCSV()<<'\n';
   
    std::cout << "\nPrint 2 tables side to side\n";
    std::cout << rat.toStringBeside(latm,"\t->\t","table\nof rat","table of latm");
    return EXIT_SUCCESS;
}